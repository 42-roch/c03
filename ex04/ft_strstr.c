/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/07 14:35:45 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/08 11:46:16 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	getlenght(char *s)
{
	int	a;

	a = 0;
	while (s[a])
		a++;
	return (a);
}

char	*copystr(char *str, int l)
{
	int		a;
	int		i;
	char	b[30000];
	char	*c;

	i = 0;
	a = l;
	while (str[a])
	{
		b[i] = str[a];
		a++;
		i++;
	}
	c = b;
	return (c);
}

char	*ft_strstr(char *str, char *to_find)
{
	int	i;
	int	a;
	int	c;

	i = 0;
	a = 0;
	c = 0;
	if (to_find[0] == '\0')
		return (str);
	while (str[i])
	{
		while (to_find[a])
		{
			if (str[i + a] == to_find[a])
				c++;
			if (c == getlenght(to_find))
				return (copystr(str, i));
			a++;
		}
		c = 0;
		a = 0;
		i++;
	}
	return (0);
}
